<!-- Contenido de la entrada -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post-inner-header">
		<div class="post-inner">
			<div class="col-md-8 post-author">
				<div class="triangulo"></div>
				<i class="fa fa-user" aria-hidden="true"></i>

				<div class="post-author-content">
					<h3 class="author-name">
						<a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php echo get_the_author_meta('first_name') . ' ' . get_the_author_meta('last_name'); ?></a>
					</h3>
					<p class="author-description">
		                <?php echo get_the_author_meta('description'); ?>
		            </p>
					<?php alperatzListSocialNetwork(); ?>
				</div><!-- post-author-content -->
			</div><!-- .post-author -->
			<div class="col-md-4 post-info">
				<div>
					<ul class="info">
						<li>
							<span class="fa fa-calendar"></span>
							<?php the_time('d/m/Y') ?>
						</li>
						<li>
							<span class="fa fa-tags"></span>
							<?php
								$category = get_the_category();
								if ($category) {
								  echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
							  }
							?>
						</li>
						<li>
							<span class="fa fa-comment"></span>
							<?php
			                    comments_number(
			                    '<span>' . __("Ningún","portallinux") . '</span> ' . __("comentario","portallinux") . '',
			                    '<span>' . __("Un","portallinux") . '</span> ' . __("comentario","portallinux") . '',
			                    '<span>%</span> ' . __("comentarios","portallinux") );?>
			                <?php _e("","portallinux"); ?>
						</li>
						<li>
							<?php edit_post_link( __( 'Edit' ), '<span class="glyphicon glyphicon-edit"></span> <span class="edit-link">', '</span>' ); ?>
						</li>
					</ul><!-- .info -->
					<div class="nextandPrevious col-md-12 text-left">
						<ul class="navnap">
							<li class="col-md-6 left">
								<?php next_post('%','<span data-toggle="tooltip" data-placement="top" title="Entrada anterior" class="glyphicon glyphicon-chevron-left"></span>', 'no'); ?>
							</li>
							<li class="col-md-6 right">
								<?php previous_post('%','<span data-toggle="tooltip" data-placement="top" title="Siguiente entrada" class="glyphicon glyphicon-chevron-right"></span>', 'no'); ?>
							</li>
						</ul>
					</div>
				</div>
			</div><!-- .post-info -->
		</div>
	</div><!-- .post-inner-header -->

	<div class="post-inner-title col-md-12">
		<h1 class="post-title"><?php the_title(); ?></h1>
	</div><!-- .post-inner-title -->


	<div class="post-inner-content content-article col-md-12">
		<header class="entry-header page-header">

		</header><!-- .entry-header -->

		<div class="entry-content">
			<?php oldPosts() ?>
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before'            => '<div class="page-links">'.__( 'Pages:', 'sparkling' ),
					'after'             => '</div>',
					'link_before'       => '<span>',
					'link_after'        => '</span>',
					'pagelink'          => '%',
					'echo'              => 1
	       		) );
	    	?>
		</div><!-- .entry-content -->

		<footer class="entry-meta">
			<?php alpheratz_ShareContent(); ?>
		</footer><!-- .entry-meta -->
	</div><!-- post-inner-content -->
</article><!-- #post-## -->
