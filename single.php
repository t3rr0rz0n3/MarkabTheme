<?php get_header(); ?>
                            <div id="primary" class="col-md-8 single" <?php echo alpheratz_SidebarPosition() ?>>
                                <?php alpheratz_TrendingArticles(); ?>
                                <main id="main">
                                    
                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                        <?php
                                            get_template_part( 'content-single', get_post_format() );
                                        ?>

                                    <?php endwhile; else: ?>

                                        <div class="">
                                            <h1>
                                                Aún no hay artículos para cargar
                                            </h1>
                                        </div>

                                    <?php endif; ?>

                                    <?php comments_template('',true); ?>
                                </main>

                            </div><!-- #primary -->

                            <div id="secondary" class="col-md-4">
                                <?php get_sidebar(); ?>
                            </div><!-- #secondary -->
<?php get_footer(); ?>
