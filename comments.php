<?php
/*
The comments page for Bones
*/
    if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
        die ('Please do not load this page directly. Thanks!');

    if ( post_password_required() ) { ?>
        <div class="alert alert-info">
            <?php _e("This post is password protected. Enter the password to view comments.","wpbootstrap"); ?>
        </div>

        <?php
        return;
    }
        ?>

<div id="comments" class="comments-area">
    <?php if ( have_comments() ) : ?>
        <?php if ( ! empty($comments_by_type['comment']) ) : ?>
            <h4 class="comments-title">
                <span class="glyphicon glyphicon-comment"></span>
                <?php
                    comments_number(
                    '<span>' . __("Ningún","wpbootstrap") . '</span> ' . __("comentario","wpbootstrap") . '',
                    '<span>' . __("Un","wpbootstrap") . '</span> ' . __("comentario","wpbootstrap") . '',
                    '<span>%</span> ' . __("comentarios","wpbootstrap") );?>
                <?php _e("en","wpbootstrap"); ?> &#8220;<?php the_title(); ?>&#8221;
            </h4>



        <ol class="comment-list">
           <?php wp_list_comments('type=comment&callback=wp_bootstrap_comments'); ?>
       </ol>

	  <?php endif; ?>

        <div class="pingback">
        <?php if ( ! empty($comments_by_type['pings']) ) : ?>
          <h3 id="pings">Trackbacks/Pingbacks</h3>

            <ol class="pinglist">
    		    <?php wp_list_comments('type=pings&callback=list_pings'); ?>

    		</ol>
        <?php endif; ?>
        </div>

        <?php if ( ! comments_open() ) : ?>
	        <p class="commetsClosed">
                <span class="glyphicon glyphicon-alert"></span><?php _e(" Comentarios cerrados","AlpheratzTheme"); ?>.
            </p>
	    <?php endif; ?>
    <?php endif; ?>


    <?php if ( comments_open() ) : ?>
	       <?php comment_form(); ?>
    <?php endif; // if you delete this the sky will fall on your head ?>
</div>
