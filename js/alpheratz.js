$(function() {
    var header = $(".navbar");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 275) {
            header.addClass("navbar-small");
        } else {
            header.removeClass("navbar-small");
        }
    });
});


// Search box toggle
$(document).ready(function(){
    $(".buscador").click(function(event){
        $("#box_animate").slideToggle('5000');
        event.preventDefault();
        $("#s").focus();
    });
});

$(document).ready(function(){
    $(".buscador").click(function(){
        $("a").attr("data-toggle");
    });
});

// Tooltip
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// popover
$('#cookie').popover()

// Show chevron up (mostrar flecha, ocultar flecha)
$(document).ready(function($){
    $(window).scroll(function () {
        if ( $(this).scrollTop() > 500 )
            $("#backtotop").fadeIn("slow");
        else
            $("#backtotop").fadeOut("slow");
        });

    $("#totop").click(function () {
        $("body,html").animate({ scrollTop: 0 }, 800 );
        return false;
    });
});

// Go to top effect
var toTop;
function toTopEffect() {
    if (document.body.scrollTop != 0 || document.documentElement.scrollTop != 0) {
        window.scrollBy(0, -15);
        toTop = setTimeout('toTopEffect()', 5);
    } else {
        clearTimeout(toTop);
    }
}

// Close cookies
function cerrarCookie(galeta) {
    $(galeta).fadeOut("slow");

}
