<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {
	// Change this to use your theme slug
	return 'options-framework-theme';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'theme-textdomain'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Styles
	$style = array(
			'style.min.css' => __('Alpheratz Theme', 'theme-textdomain'),
			'css/styles/style_red.css' => __( 'Red', 'theme-textdomain'),
			'css/styles/style_green.css' => __( 'Green', 'theme-textdomain'),
			'css/styles/style_yellow.css' => __( 'Yellow', 'theme-textdomain'),
			'css/styles/style_blue.css' => __( 'Blue', 'theme-textdomain'),
			'css/styles/style_purple.css' => __( 'Purple', 'theme-textdomain'),
			'css/styles/style_aqua.css' => __( 'Aqua', 'theme-textdomain'),
			'css/styles/style_orange.css' => __( 'Orange', 'theme-textdomain')
	);

	// Glyphicons
	$glyphicons = array(
		'glyphicon-adjust' => __( 'glyphicon glyphicon-adjust', 'theme-textdomain' ),
		'glyphicon-alert' => __( 'glyphicon glyphicon-alert', 'theme-textdomain' ),
		'glyphicon-align-center' => __( 'glyphicon glyphicon-align-center', 'theme-textdomain' ),
		'glyphicon-align-justify' => __( 'glyphicon glyphicon-align-justify', 'theme-textdomain' ),
		'glyphicon-align-left' => __( 'glyphicon glyphicon-align-left', 'theme-textdomain' ),
		'glyphicon-align-right' => __( 'glyphicon glyphicon-align-right', 'theme-textdomain' ),
		'glyphicon-apple' => __( 'glyphicon glyphicon-apple', 'theme-textdomain' ),
		'glyphicon-arrow-down' => __( 'glyphicon glyphicon-arrow-down', 'theme-textdomain' ),
		'glyphicon-arrow-left' => __( 'glyphicon glyphicon-arrow-left', 'theme-textdomain' ),
		'glyphicon-arrow-right' => __( 'glyphicon glyphicon-arrow-right', 'theme-textdomain' ),
		'glyphicon-arrow-up' => __( 'glyphicon glyphicon-arrow-up', 'theme-textdomain' ),
		'glyphicon-asterisk' => __( 'glyphicon glyphicon-asterisk', 'theme-textdomain' ),
		'glyphicon-baby-formula' => __( 'glyphicon glyphicon-baby-formula', 'theme-textdomain' ),
		'glyphicon-backward' => __( 'glyphicon glyphicon-backward', 'theme-textdomain' ),
		'glyphicon-ban-circle' => __( 'glyphicon glyphicon-ban-circle', 'theme-textdomain' ),
		'glyphicon-barcode' => __( 'glyphicon glyphicon-barcode', 'theme-textdomain' ),
		'glyphicon-bed' => __( 'glyphicon glyphicon-bed', 'theme-textdomain' ),
		'glyphicon-bell' => __( 'glyphicon glyphicon-bell', 'theme-textdomain' ),
		'glyphicon-bishop' => __( 'glyphicon glyphicon-bishop', 'theme-textdomain' ),
		'glyphicon-bitcoin' => __( 'glyphicon glyphicon-bitcoin', 'theme-textdomain' ),
		'glyphicon-blackboard' => __( 'glyphicon glyphicon-blackboard', 'theme-textdomain' ),
		'glyphicon-bold' => __( 'glyphicon glyphicon-bold', 'theme-textdomain' ),
		'glyphicon-book' => __( 'glyphicon glyphicon-book', 'theme-textdomain' ),
		'glyphicon-bookmark' => __( 'glyphicon glyphicon-bookmark', 'theme-textdomain' ),
		'glyphicon-briefcase' => __( 'glyphicon glyphicon-briefcase', 'theme-textdomain' ),
		'glyphicon-btc' => __( 'glyphicon glyphicon-btc', 'theme-textdomain' ),
		'glyphicon-bullhorn' => __( 'glyphicon glyphicon-bullhorn', 'theme-textdomain' ),
		'glyphicon-calendar' => __( 'glyphicon glyphicon-calendar', 'theme-textdomain' ),
		'glyphicon-camera' => __( 'glyphicon glyphicon-camera', 'theme-textdomain' ),
		'glyphicon-cd' => __( 'glyphicon glyphicon-cd', 'theme-textdomain' ),
		'glyphicon-certificate' => __( 'glyphicon glyphicon-certificate', 'theme-textdomain' ),
		'glyphicon-check' => __( 'glyphicon glyphicon-check', 'theme-textdomain' ),
		'glyphicon-chevron-down' => __( 'glyphicon glyphicon-chevron-down', 'theme-textdomain' ),
		'glyphicon-chevron-left' => __( 'glyphicon glyphicon-chevron-left', 'theme-textdomain' ),
		'glyphicon-chevron-right' => __( 'glyphicon glyphicon-chevron-right', 'theme-textdomain' ),
		'glyphicon-chevron-up' => __( 'glyphicon glyphicon-chevron-up', 'theme-textdomain' ),
		'glyphicon-circle-arrow-down' => __( 'glyphicon glyphicon-circle-arrow-down', 'theme-textdomain' ),
		'glyphicon-circle-arrow-left' => __( 'glyphicon glyphicon-circle-arrow-left', 'theme-textdomain' ),
		'glyphicon-circle-arrow-right' => __( 'glyphicon glyphicon-circle-arrow-right', 'theme-textdomain' ),
		'glyphicon-circle-arrow-up' => __( 'glyphicon glyphicon-circle-arrow-up', 'theme-textdomain' ),
		'glyphicon-cloud' => __( 'glyphicon glyphicon-cloud', 'theme-textdomain' ),
		'glyphicon-cloud-download' => __( 'glyphicon glyphicon-cloud-download', 'theme-textdomain' ),
		'glyphicon-cloud-upload' => __( 'glyphicon glyphicon-cloud-upload', 'theme-textdomain' ),
		'glyphicon-cog' => __( 'glyphicon glyphicon-cog', 'theme-textdomain' ),
		'glyphicon-collapse-down' => __( 'glyphicon glyphicon-collapse-down', 'theme-textdomain' ),
		'glyphicon-collapse-up' => __( 'glyphicon glyphicon-collapse-up', 'theme-textdomain' ),
		'glyphicon-comment' => __( 'glyphicon glyphicon-comment', 'theme-textdomain' ),
		'glyphicon-compressed' => __( 'glyphicon glyphicon-compressed', 'theme-textdomain' ),
		'glyphicon-console' => __( 'glyphicon glyphicon-console', 'theme-textdomain' ),
		'glyphicon-copy' => __( 'glyphicon glyphicon-copy', 'theme-textdomain' ),
		'glyphicon-copyright-mark' => __( 'glyphicon glyphicon-copyright-mark', 'theme-textdomain' ),
		'glyphicon-credit-card' => __( 'glyphicon glyphicon-credit-card', 'theme-textdomain' ),
		'glyphicon-cutlery' => __( 'glyphicon glyphicon-cutlery', 'theme-textdomain' ),
		'glyphicon-dashboard' => __( 'glyphicon glyphicon-dashboard', 'theme-textdomain' ),
		'glyphicon-download' => __( 'glyphicon glyphicon-download', 'theme-textdomain' ),
		'glyphicon-download-alt' => __( 'glyphicon glyphicon-download-alt', 'theme-textdomain' ),
		'glyphicon-duplicate' => __( 'glyphicon glyphicon-duplicate', 'theme-textdomain' ),
		'glyphicon-earphone' => __( 'glyphicon glyphicon-earphone', 'theme-textdomain' ),
		'glyphicon-edit' => __( 'glyphicon glyphicon-edit', 'theme-textdomain' ),
		'glyphicon-education' => __( 'glyphicon glyphicon-education', 'theme-textdomain' ),
		'glyphicon-eject' => __( 'glyphicon glyphicon-eject', 'theme-textdomain' ),
		'glyphicon-envelope' => __( 'glyphicon glyphicon-envelope', 'theme-textdomain' ),
		'glyphicon-equalizer' => __( 'glyphicon glyphicon-equalizer', 'theme-textdomain' ),
		'glyphicon-erase' => __( 'glyphicon glyphicon-erase', 'theme-textdomain' ),
		'glyphicon-eur' => __( 'glyphicon glyphicon-eur', 'theme-textdomain' ),
		'glyphicon-euro' => __( 'glyphicon glyphicon-euro', 'theme-textdomain' ),
		'glyphicon-exclamation-sign' => __( 'glyphicon glyphicon-exclamation-sign', 'theme-textdomain' ),
		'glyphicon-expand' => __( 'glyphicon glyphicon-expand', 'theme-textdomain' ),
		'glyphicon-export' => __( 'glyphicon glyphicon-export', 'theme-textdomain' ),
		'glyphicon-eye-close' => __( 'glyphicon glyphicon-eye-close', 'theme-textdomain' ),
		'glyphicon-eye-open' => __( 'glyphicon glyphicon-eye-open', 'theme-textdomain' ),
		'glyphicon-facetime-video' => __( 'glyphicon glyphicon-facetime-video', 'theme-textdomain' ),
		'glyphicon-fast-backward' => __( 'glyphicon glyphicon-fast-backward', 'theme-textdomain' ),
		'glyphicon-fast-forward' => __( 'glyphicon glyphicon-fast-forward', 'theme-textdomain' ),
		'glyphicon-file' => __( 'glyphicon glyphicon-file', 'theme-textdomain' ),
		'glyphicon-film' => __( 'glyphicon glyphicon-film', 'theme-textdomain' ),
		'glyphicon-filter' => __( 'glyphicon glyphicon-filter', 'theme-textdomain' ),
		'glyphicon-fire' => __( 'glyphicon glyphicon-fire', 'theme-textdomain' ),
		'glyphicon-flag' => __( 'glyphicon glyphicon-flag', 'theme-textdomain' ),
		'glyphicon glyphicon-flash' => __( 'glyphicon glyphicon-flash', 'theme-textdomain' ),
		'glyphicon-floppy-disk' => __( 'glyphicon glyphicon-floppy-disk', 'theme-textdomain' ),
		'glyphicon-floppy-open' => __( 'glyphicon glyphicon-floppy-open', 'theme-textdomain' ),
		'glyphicon-floppy-remove' => __( 'glyphicon glyphicon-floppy-remove', 'theme-textdomain' ),
		'glyphicon-floppy-save' => __( 'glyphicon glyphicon-floppy-save', 'theme-textdomain' ),
		'glyphicon-floppy-saved' => __( 'glyphicon glyphicon-floppy-saved', 'theme-textdomain' ),
		'glyphicon-folder-close' => __( 'glyphicon glyphicon-folder-close', 'theme-textdomain' ),
		'glyphicon-folder-open' => __( 'glyphicon glyphicon-folder-open', 'theme-textdomain' ),
		'glyphicon-font' => __( 'glyphicon glyphicon-font', 'theme-textdomain' ),
		'glyphicon-forward' => __( 'glyphicon glyphicon-forward', 'theme-textdomain' ),
		'glyphicon-fullscreen' => __( 'glyphicon glyphicon-fullscreen', 'theme-textdomain' ),
		'glyphicon-gbp' => __( 'glyphicon glyphicon-gbp', 'theme-textdomain' ),
		'glyphicon-gift' => __( 'glyphicon glyphicon-gift', 'theme-textdomain' ),
		'glyphicon-glass' => __( 'glyphicon glyphicon-glass', 'theme-textdomain' ),
		'glyphicon-globe' => __( 'glyphicon glyphicon-globe', 'theme-textdomain' ),
		'glyphicon-grain' => __( 'glyphicon glyphicon-grain', 'theme-textdomain' ),
		'glyphicon-hand-down' => __( 'glyphicon glyphicon-hand-down', 'theme-textdomain' ),
		'glyphicon-hand-left' => __( 'glyphicon glyphicon-hand-left', 'theme-textdomain' ),
		'glyphicon-hand-right' => __( 'glyphicon glyphicon-hand-right', 'theme-textdomain' ),
		'glyphicon-hand-up' => __( 'glyphicon glyphicon-hand-up', 'theme-textdomain' ),
		'glyphicon-hdd' => __( 'glyphicon glyphicon-hdd', 'theme-textdomain' ),
		'glyphicon-hd-video' => __( 'glyphicon glyphicon-hd-video', 'theme-textdomain' ),
		'glyphicon-header' => __( 'glyphicon glyphicon-header', 'theme-textdomain' ),
		'glyphicon-headphones' => __( 'glyphicon glyphicon-headphones', 'theme-textdomain' ),
		'glyphicon-heart' => __( 'glyphicon glyphicon-heart', 'theme-textdomain' ),
		'glyphicon-heart-empty' => __( 'glyphicon glyphicon-heart-empty', 'theme-textdomain' ),
		'glyphicon-home' => __( 'glyphicon glyphicon-home', 'theme-textdomain' ),
		'glyphicon-hourglass' => __( 'glyphicon glyphicon-hourglass', 'theme-textdomain' ),
		'glyphicon-ice-lolly' => __( 'glyphicon glyphicon-ice-lolly', 'theme-textdomain' ),
		'glyphicon-ice-lolly-tasted' => __( 'glyphicon glyphicon-ice-lolly-tasted', 'theme-textdomain' ),
		'glyphicon-import' => __( 'glyphicon glyphicon-import', 'theme-textdomain' ),
		'glyphicon-inbox' => __( 'glyphicon glyphicon-inbox', 'theme-textdomain' ),
		'glyphicon-indent-left' => __( 'glyphicon glyphicon-indent-left', 'theme-textdomain' ),
		'glyphicon-indent-right' => __( 'glyphicon glyphicon-indent-right', 'theme-textdomain' ),
		'glyphicon-info-sign' => __( 'glyphicon glyphicon-info-sign', 'theme-textdomain' ),
		'glyphicon-italic' => __( 'glyphicon glyphicon-italic', 'theme-textdomain' ),
		'glyphicon-jpy' => __( 'glyphicon glyphicon-jpy', 'theme-textdomain' ),
		'glyphicon-king' => __( 'glyphicon glyphicon-king', 'theme-textdomain' ),
		'glyphicon-knight' => __( 'glyphicon glyphicon-knight', 'theme-textdomain' ),
		'glyphicon-lamp' => __( 'glyphicon glyphicon-lamp', 'theme-textdomain' ),
		'glyphicon-leaf' => __( 'glyphicon glyphicon-leaf', 'theme-textdomain' ),
		'glyphicon-level-up' => __( 'glyphicon glyphicon-level-up', 'theme-textdomain' ),
		'glyphicon-link' => __( 'glyphicon glyphicon-link', 'theme-textdomain' ),
		'glyphicon-list' => __( 'glyphicon glyphicon-list', 'theme-textdomain' ),
		'glyphicon-list-alt' => __( 'glyphicon glyphicon-list-alt', 'theme-textdomain' ),
		'glyphicon-lock' => __( 'glyphicon glyphicon-lock', 'theme-textdomain' ),
		'glyphicon-log-in' => __( 'glyphicon glyphicon-log-in', 'theme-textdomain' ),
		'glyphicon-log-out' => __( 'glyphicon glyphicon-log-out', 'theme-textdomain' ),
		'glyphicon-magnet' => __( 'glyphicon glyphicon-magnet', 'theme-textdomain' ),
		'glyphicon-map-marker' => __( 'glyphicon glyphicon-map-marker', 'theme-textdomain' ),
		'glyphicon-menu-down' => __( 'glyphicon glyphicon-menu-down', 'theme-textdomain' ),
		'glyphicon-menu-hamburger' => __( 'glyphicon glyphicon-menu-hamburger', 'theme-textdomain' ),
		'glyphicon-menu-left' => __( 'glyphicon glyphicon-menu-left', 'theme-textdomain' ),
		'glyphicon-menu-right' => __( 'glyphicon glyphicon-menu-right', 'theme-textdomain' ),
		'glyphicon-menu-up' => __( 'glyphicon glyphicon-menu-up', 'theme-textdomain' ),
		'glyphicon-minus' => __( 'glyphicon glyphicon-minus', 'theme-textdomain' ),
		'glyphicon-minus-sign' => __( 'glyphicon glyphicon-minus-sign', 'theme-textdomain' ),
		'glyphicon-modal-window' => __( 'glyphicon glyphicon-modal-window', 'theme-textdomain' ),
		'glyphicon-move' => __( 'glyphicon glyphicon-move', 'theme-textdomain' ),
		'glyphicon-music' => __( 'glyphicon glyphicon-music', 'theme-textdomain' ),
		'glyphicon-new-window' => __( 'glyphicon glyphicon-new-window', 'theme-textdomain' ),
		'glyphicon-object-align-bottom' => __( 'glyphicon glyphicon-object-align-bottom', 'theme-textdomain' ),
		'glyphicon-object-align-horizontal' => __( 'glyphicon glyphicon-object-align-horizontal', 'theme-textdomain' ),
		'glyphicon-object-align-left' => __( 'glyphicon glyphicon-object-align-left', 'theme-textdomain' ),
		'glyphicon-object-align-right' => __( 'glyphicon glyphicon-object-align-right', 'theme-textdomain' ),
		'glyphicon-object-align-top' => __( 'glyphicon glyphicon-object-align-top', 'theme-textdomain' ),
		'glyphicon-object-align-vertical' => __( 'glyphicon glyphicon-object-align-vertical', 'theme-textdomain' ),
		'glyphicon-off' => __( 'glyphicon glyphicon-off', 'theme-textdomain' ),
		'glyphicon-oil' => __( 'glyphicon glyphicon-oil', 'theme-textdomain' ),
		'glyphicon-ok' => __( 'glyphicon glyphicon-ok', 'theme-textdomain' ),
		'glyphicon-ok-circle' => __( 'glyphicon glyphicon-ok-circle', 'theme-textdomain' ),
		'glyphicon-ok-sign' => __( 'glyphicon glyphicon-ok-sign', 'theme-textdomain' ),
		'glyphicon-open' => __( 'glyphicon glyphicon-open', 'theme-textdomain' ),
		'glyphicon-open-file' => __( 'glyphicon glyphicon-open-file', 'theme-textdomain' ),
		'glyphicon-option-horizontal' => __( 'glyphicon glyphicon-option-horizontal', 'theme-textdomain' ),
		'glyphicon-option-vertical' => __( 'glyphicon glyphicon-option-vertical', 'theme-textdomain' ),
		'glyphicon-paperclip' => __( 'glyphicon glyphicon-paperclip', 'theme-textdomain' ),
		'glyphicon-paste' => __( 'glyphicon glyphicon-paste', 'theme-textdomain' ),
		'glyphicon-pause' => __( 'glyphicon glyphicon-pause', 'theme-textdomain' ),
		'glyphicon-pawn' => __( 'glyphicon glyphicon-pawn', 'theme-textdomain' ),
		'glyphicon-pencil' => __( 'glyphicon glyphicon-pencil', 'theme-textdomain' ),
		'glyphicon-phone' => __( 'glyphicon glyphicon-phone', 'theme-textdomain' ),
		'glyphicon-phone-alt' => __( 'glyphicon glyphicon-phone-alt', 'theme-textdomain' ),
		'glyphicon-picture' => __( 'glyphicon glyphicon-picture', 'theme-textdomain' ),
		'glyphicon-piggy-bank' => __( 'glyphicon glyphicon-piggy-bank', 'theme-textdomain' ),
		'glyphicon-plane' => __( 'glyphicon glyphicon-plane', 'theme-textdomain' ),
		'glyphicon-play' => __( 'glyphicon glyphicon-play', 'theme-textdomain' ),
		'glyphicon-play-circle' => __( 'glyphicon glyphicon-play-circle', 'theme-textdomain' ),
		'glyphicon-plus' => __( 'glyphicon glyphicon-plus', 'theme-textdomain' ),
		'glyphicon-plus-sign' => __( 'glyphicon glyphicon-plus-sign', 'theme-textdomain' ),
		'glyphicon-print' => __( 'glyphicon glyphicon-print', 'theme-textdomain' ),
		'glyphicon-pushpin' => __( 'glyphicon glyphicon-pushpin', 'theme-textdomain' ),
		'glyphicon-qrcode' => __( 'glyphicon glyphicon-qrcode', 'theme-textdomain' ),
		'glyphicon-queen' => __( 'glyphicon glyphicon-queen', 'theme-textdomain' ),
		'glyphicon-question-sign' => __( 'glyphicon glyphicon-question-sign', 'theme-textdomain' ),
		'glyphicon-random' => __( 'glyphicon glyphicon-random', 'theme-textdomain' ),
		'glyphicon-record' => __( 'glyphicon glyphicon-record', 'theme-textdomain' ),
		'glyphicon-refresh' => __( 'glyphicon glyphicon-refresh', 'theme-textdomain' ),
		'glyphicon-registration-mark' => __( 'glyphicon glyphicon-registration-mark', 'theme-textdomain' ),
		'glyphicon-remove' => __( 'glyphicon glyphicon-remove', 'theme-textdomain' ),
		'glyphicon-remove-circle' => __( 'glyphicon glyphicon-remove-circle', 'theme-textdomain' ),
		'glyphicon-remove-sign' => __( 'glyphicon glyphicon-remove-sign', 'theme-textdomain' ),
		'glyphicon-repeat' => __( 'glyphicon glyphicon-repeat', 'theme-textdomain' ),
		'glyphicon-resize-full' => __( 'glyphicon glyphicon-resize-full', 'theme-textdomain' ),
		'glyphicon-resize-horizontal' => __( 'glyphicon glyphicon-resize-horizontal', 'theme-textdomain' ),
		'glyphicon-resize-small' => __( 'glyphicon glyphicon-resize-small', 'theme-textdomain' ),
		'glyphicon-resize-vertical' => __( 'glyphicon glyphicon-resize-vertical', 'theme-textdomain' ),
		'glyphicon-retweet' => __( 'glyphicon glyphicon-retweet', 'theme-textdomain' ),
		'glyphicon-road' => __( 'glyphicon glyphicon-road', 'theme-textdomain' ),
		'glyphicon-rub' => __( 'glyphicon glyphicon-rub', 'theme-textdomain' ),
		'glyphicon-ruble' => __( 'glyphicon glyphicon-ruble', 'theme-textdomain' ),
		'glyphicon-save' => __( 'glyphicon glyphicon-save', 'theme-textdomain' ),
		'glyphicon-saved' => __( 'glyphicon glyphicon-saved', 'theme-textdomain' ),
		'glyphicon-save-file' => __( 'glyphicon glyphicon-save-file', 'theme-textdomain' ),
		'glyphicon-scale' => __( 'glyphicon glyphicon-scale', 'theme-textdomain' ),
		'glyphicon-scissors' => __( 'glyphicon glyphicon-scissors', 'theme-textdomain' ),
		'glyphicon-screenshot' => __( 'glyphicon glyphicon-screenshot', 'theme-textdomain' ),
		'glyphicon-sd-video' => __( 'glyphicon glyphicon-sd-video', 'theme-textdomain' ),
		'glyphicon-search' => __( 'glyphicon glyphicon-search', 'theme-textdomain' ),
		'glyphicon-send' => __( 'glyphicon glyphicon-send', 'theme-textdomain' ),
		'glyphicon-share' => __( 'glyphicon glyphicon-share', 'theme-textdomain' ),
		'glyphicon-share-alt' => __( 'glyphicon glyphicon-share-alt', 'theme-textdomain' ),
		'glyphicon-shopping-cart' => __( 'glyphicon glyphicon-shopping-cart', 'theme-textdomain' ),
		'glyphicon-signal' => __( 'glyphicon glyphicon-signal', 'theme-textdomain' ),
		'glyphicon-sort' => __( 'glyphicon glyphicon-sort', 'theme-textdomain' ),
		'glyphicon-sort-by-alphabet' => __( 'glyphicon glyphicon-sort-by-alphabet', 'theme-textdomain' ),
		'glyphicon-sort-by-alphabet-alt' => __( 'glyphicon glyphicon-sort-by-alphabet-alt', 'theme-textdomain' ),
		'glyphicon-sort-by-attributes' => __( 'glyphicon glyphicon-sort-by-attributes', 'theme-textdomain' ),
		'glyphicon-sort-by-attributes-alt' => __( 'glyphicon glyphicon-sort-by-attributes-alt', 'theme-textdomain' ),
		'glyphicon-sort-by-order' => __( 'glyphicon glyphicon-sort-by-order', 'theme-textdomain' ),
		'glyphicon-sort-by-order-alt' => __( 'glyphicon glyphicon-sort-by-order-alt', 'theme-textdomain' ),
		'glyphicon-sound-5-1' => __( 'glyphicon glyphicon-sound-5-1', 'theme-textdomain' ),
		'glyphicon-sound-6-1' => __( 'glyphicon glyphicon-sound-6-1', 'theme-textdomain' ),
		'glyphicon-sound-7-1' => __( 'glyphicon glyphicon-sound-7-1', 'theme-textdomain' ),
		'glyphicon-sound-dolby' => __( 'glyphicon glyphicon-sound-dolby', 'theme-textdomain' ),
		'glyphicon-sound-stereo' => __( 'glyphicon glyphicon-sound-stereo', 'theme-textdomain' ),
		'glyphicon-star' => __( 'glyphicon glyphicon-star', 'theme-textdomain' ),
		'glyphicon-star-empty' => __( 'glyphicon glyphicon-star-empty', 'theme-textdomain' ),
		'glyphicon-stats' => __( 'glyphicon glyphicon-stats', 'theme-textdomain' ),
		'glyphicon-step-backward' => __( 'glyphicon glyphicon-step-backward', 'theme-textdomain' ),
		'glyphicon-step-forward' => __( 'glyphicon glyphicon-step-forward', 'theme-textdomain' ),
		'glyphicon-stop' => __( 'glyphicon glyphicon-stop', 'theme-textdomain' ),
		'glyphicon-subscript' => __( 'glyphicon glyphicon-subscript', 'theme-textdomain' ),
		'glyphicon-subtitles' => __( 'glyphicon glyphicon-subtitles', 'theme-textdomain' ),
		'glyphicon-sunglasses' => __( 'glyphicon glyphicon-sunglasses', 'theme-textdomain' ),
		'glyphicon-superscript' => __( 'glyphicon glyphicon-superscript', 'theme-textdomain' ),
		'glyphicon-tag' => __( 'glyphicon glyphicon-tag', 'theme-textdomain' ),
		'glyphicon-tags' => __( 'glyphicon glyphicon-tags', 'theme-textdomain' ),
		'glyphicon-tasks' => __( 'glyphicon glyphicon-tasks', 'theme-textdomain' ),
		'glyphicon-tent' => __( 'glyphicon glyphicon-tent', 'theme-textdomain' ),
		'glyphicon-text-background' => __( 'glyphicon glyphicon-text-background', 'theme-textdomain' ),
		'glyphicon-text-color' => __( 'glyphicon glyphicon-text-color', 'theme-textdomain' ),
		'glyphicon-text-height' => __( 'glyphicon glyphicon-text-height', 'theme-textdomain' ),
		'glyphicon-text-size' => __( 'glyphicon glyphicon-text-size', 'theme-textdomain' ),
		'glyphicon-text-width' => __( 'glyphicon glyphicon-text-width', 'theme-textdomain' ),
		'glyphicon-th' => __( 'glyphicon glyphicon-th', 'theme-textdomain' ),
		'glyphicon-th-large' => __( 'glyphicon glyphicon-th-large', 'theme-textdomain' ),
		'glyphicon-th-list' => __( 'glyphicon glyphicon-th-list', 'theme-textdomain' ),
		'glyphicon-thumbs-down' => __( 'glyphicon glyphicon-thumbs-down', 'theme-textdomain' ),
		'glyphicon-thumbs-up' => __( 'glyphicon glyphicon-thumbs-up', 'theme-textdomain' ),
		'glyphicon-time' => __( 'glyphicon glyphicon-time', 'theme-textdomain' ),
		'glyphicon-tint' => __( 'glyphicon glyphicon-tint', 'theme-textdomain' ),
		'glyphicon-tower' => __( 'glyphicon glyphicon-tower', 'theme-textdomain' ),
		'glyphicon-transfer' => __( 'glyphicon glyphicon-transfer', 'theme-textdomain' ),
		'glyphicon-trash' => __( 'glyphicon glyphicon-trash', 'theme-textdomain' ),
		'glyphicon-tree-conifer' => __( 'glyphicon glyphicon-tree-conifer', 'theme-textdomain' ),
		'glyphicon-tree-deciduous' => __( 'glyphicon glyphicon-tree-deciduous', 'theme-textdomain' ),
		'glyphicon-triangle-bottom' => __( 'glyphicon glyphicon-triangle-bottom', 'theme-textdomain' ),
		'glyphicon-triangle-left' => __( 'glyphicon glyphicon-triangle-left', 'theme-textdomain' ),
		'glyphicon-triangle-right' => __( 'glyphicon glyphicon-triangle-right', 'theme-textdomain' ),
		'glyphicon-triangle-top' => __( 'glyphicon glyphicon-triangle-top', 'theme-textdomain' ),
		'glyphicon-unchecked' => __( 'glyphicon glyphicon-unchecked', 'theme-textdomain' ),
		'glyphicon-upload' => __( 'glyphicon glyphicon-upload', 'theme-textdomain' ),
		'glyphicon-usd' => __( 'glyphicon glyphicon-usd', 'theme-textdomain' ),
		'glyphicon-user' => __( 'glyphicon glyphicon-user', 'theme-textdomain' ),
		'glyphicon-volume-down' => __( 'glyphicon glyphicon-volume-down', 'theme-textdomain' ),
		'glyphicon-volume-off' => __( 'glyphicon glyphicon-volume-off', 'theme-textdomain' ),
		'glyphicon-volume-up' => __( 'glyphicon glyphicon-volume-up', 'theme-textdomain' ),
		'glyphicon-warning-sign' => __( 'glyphicon glyphicon-warning-sign', 'theme-textdomain' ),
		'glyphicon-wrench' => __( 'glyphicon glyphicon-wrench', 'theme-textdomain' ),
		'glyphicon-xbt' => __( 'glyphicon glyphicon-xbt', 'theme-textdomain' ),
		'glyphicon-yen' => __( 'glyphicon glyphicon-yen', 'theme-textdomain' ),
		'glyphicon-zoom-in' => __( 'glyphicon glyphicon-zoom-in', 'theme-textdomain' ),
		'glyphicon-zoom-out' => __( 'glyphicon glyphicon-zoom-out', 'theme-textdomain' )
	);

	// Multicheck Array
	/*$multicheck_array = array(
		'one' => __( 'French Toast', 'theme-textdomain' ),
		'two' => __( 'Pancake', 'theme-textdomain' ),
		'three' => __( 'Omelette', 'theme-textdomain' ),
		'four' => __( 'Crepe', 'theme-textdomain' ),
		'five' => __( 'Waffle', 'theme-textdomain' )
	);*/

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => false
	);

	// License type
	$license_options = array(
		'cc-sa' => __( 'cc-sa', 'theme-textdomain' ),
		'cc-nc-eu' => __( 'cc-nc-eu', 'theme-textdomain' ),
		'cc-zero' => __( 'cc-zero', 'theme-textdomain' ),
		'cc-share' => __( 'cc-share', 'theme-textdomain' ),
		'cc-by' => __( 'cc-by', 'theme-textdomain' ),
		'cc-nc-jp' => __( 'cc-nc-jp', 'theme-textdomain' ),
		'cc-pd' => __( 'cc-pd', 'theme-textdomain' ),
		'cc-cc' => __( 'cc-cc', 'theme-textdomain' ),
		'cc-sampling' => __( 'cc-sampling', 'theme-textdomain' ),
		'cc-pd-alt' => __( 'cc-pd-alt', 'theme-textdomain' ),
		'cc-nd' => __( 'cc-nd', 'theme-textdomain' ),
		'cc-nc' => __( 'cc-nc', 'theme-textdomain' ),
		'cc-remix' => __( 'cc-remix', 'theme-textdomain' )
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}


	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/inc/AlpheratzOptions/images/';

	$options = array();

	/* CONFIGURACION BASICA */
	$options[] = array(
		'name' => __( 'Configuración bàsica', 'theme-textdomain' ),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __( 'Glyphicons', 'theme-textdomain' ),
		'desc' => __( 'Cambia el Glyphicon del titulo principal.', 'theme-textdomain' ),
		'id' => 'glyphicon',
		'std' => 'glyphicon glyphicon-flash',
		'type' => 'select',
		'options' => $glyphicons
	);

	$options[] = array(
		'name' => __( 'Título principal de la web: Primera palabra', 'theme-textdomain' ),
		'desc' => __( 'En el menu principal nuestra el titulo principal del blog en dos colores.', 'theme-textdomain' ),
		'id' => 'firstword',
		'std' => 'Alpheratz',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Título principal de la web: Segunda palabra', 'theme-textdomain' ),
		'desc' => __( 'En el menu principal nuestra el titulo principal del blog en dos colores.', 'theme-textdomain' ),
		'id' => 'secondword',
		'std' => 'Theme',
		'class' => 'mini',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Activar aviso cookies', 'theme-textdomain' ),
		'desc' => __( 'Muestra un aviso de las cookies (no molesto)', 'theme-textdomain' ),
		'id' => 'cookies',
		'std' => '0',
		'type' => 'checkbox'
	);

	$options[] = array(
		'name' => __( 'Activar Breadcrumbs', 'theme-textdomain' ),
		'desc' => __( 'Activar breadcrumbs para saber donde estás.', 'theme-textdomain' ),
		'id' => 'breadcrumb',
		'std' => '0',
		'type' => 'checkbox'
	);

	$options[] = array(
		'name' => __( 'Activar "Subir arriba"', 'theme-textdomain' ),
		'desc' => __( 'Añade enlace en el footer para subir arriba (con efecto).', 'theme-textdomain' ),
		'id' => 'totop',
		'std' => '0',
		'type' => 'checkbox'
	);

	$options[] = array(
		'name' => __( 'Activar "Infinite Scroll"', 'theme-textdomain' ),
		'desc' => __( 'Activa este efecto para que carguen las entradas automáticamente.', 'theme-textdomain' ),
		'id' => 'infinitescroll',
		'std' => '1',
		'type' => 'checkbox'
	);

	$options[] = array(
		'name' => __( 'Activar "Lo más visto"', 'theme-textdomain' ),
		'desc' => __( 'Añade una lista en la parte superior de la entrada de los 3 posts más visitados.', 'theme-textdomain' ),
		'id' => 'trendpost',
		'std' => '0',
		'type' => 'checkbox'
	);

	$options[] = array(
		'name' => __( 'Activar compartir con redes sociales', 'theme-textdomain' ),
		'desc' => __( 'Añade al final de cada entrada la opción de compartir con redes sociales (Facebook, Twitter, Google+ y Email). ATENCIÓN: Está en fase alpha, no funciona del todo bien.', 'theme-textdomain' ),
		'id' => 'sharecontent',
		'std' => '0',
		'type' => 'checkbox'
	);

	/* CONFIGURACION AVANZADA */
	$options[] = array(
		'name' => __( 'Configuracion Avanzada', 'theme-textdomain' ),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => "Posición de la barra lateral",
		'desc' => "Izquierda o derecha.",
		'id' => "sidebar_position",
		'std' => "sidebar_left",
		'type' => "images",
		'options' => array(
			'sidebar_left' => $imagepath . '2cl.png',
			/*'col-md-12' => $imagepath . '1col.png',*/
			'sidebar_right' => $imagepath . '2cr.png'
		)
	);

	$options[] = array(
		'name' => __( 'Style', 'theme-textdomain' ),
		'desc' => __( 'Cambia el estilo principal.', 'theme-textdomain' ),
		'id' => 'style',
		'std' => 'style.min.css',
		'type' => 'select',
		'options' => $style
	);

	$options[] = array(
		'name' =>  __( 'Fondo del tema', 'theme-textdomain' ),
		'desc' => __( 'Cambia el fondo del tema por CSS, por defecto: #F1F1F1.', 'theme-textdomain' ),
		'id' => 'example_background',
		'std' => $background_defaults,
		'type' => 'background'
	);

	$options[] = array(
		'name' => __( 'Favicon', 'theme-textdomain' ),
		'desc' => __( 'Selecciona una imagen .ico para usar de Favicon', 'theme-textdomain' ),
		'id' => 'favicon',
		'type' => 'upload'
	);

	/* TIPOGRAFIA */
	$options[] = array(
		'name' => __( 'Tipografía', 'theme-textdomain' ),
		'type' => 'heading'
	);

	$options[] = array( 'name' => __( 'Typography', 'theme-textdomain' ),
		'desc' => __( 'Example typography.', 'theme-textdomain' ),
		'id' => "example_typography",
		'std' => $typography_defaults,
		'type' => 'typography'
	);

	/* LICENCIA */
	$options[] = array(
		'name' => __( 'Licencia', 'theme-textdomain' ),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __( 'Selecciona tu licencia', 'theme-textdomain' ),
		'desc' => __( 'Marca la licencia que más te guste. Más info: http://cc-icons.github.io/icons/', 'theme-textdomain' ),
		'id' => 'license',
		'std' => $license_options, // These items get checked by default
		'type' => 'multicheck',
		'options' => $license_options
	);

	/* FOOTER */
	$options[] = array(
		'name' => __( 'Footer', 'theme-textdomain' ),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __( 'Copyright', 'theme-textdomain' ),
		'desc' => __( 'Para añadir el Copyright, aparece en la parte inferior derecha junto al nombre de la plantilla. Puedes usar etiquetas HTML como <strong> o <font>.', 'theme-textdomain' ),
		'id' => 'copyright',
		'std' => 'Nombre Autor',
		'type' => 'textarea'
	);

	$options[] = array(
		'name' => __( 'Enlace Twitter', 'theme-textdomain' ),
		'desc' => __( 'Añadir icono social.', 'theme-textdomain' ),
		'id' => 'social_tw',
		'std' => '#',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Enlace Facebook', 'theme-textdomain' ),
		'desc' => __( 'Añadir icono social.', 'theme-textdomain' ),
		'id' => 'social_fb',
		'std' => '#',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Enlace Google+', 'theme-textdomain' ),
		'desc' => __( 'Añadir icono social.', 'theme-textdomain' ),
		'id' => 'social_gp',
		'std' => '#',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Enlace GitHub', 'theme-textdomain' ),
		'desc' => __( 'Añadir icono social.', 'theme-textdomain' ),
		'id' => 'social_gh',
		'std' => '#',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Enlace Canal Telegram', 'theme-textdomain' ),
		'desc' => __( 'Añadir icono social.', 'theme-textdomain' ),
		'id' => 'social_tg',
		'std' => '#',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Enlace RSS', 'theme-textdomain' ),
		'desc' => __( 'Añadir icono social.', 'theme-textdomain' ),
		'id' => 'social_rs',
		'std' => '#',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Enlace a la licencia de tu contenido', 'theme-textdomain' ),
		'desc' => __( 'Añadir icono de licencia.', 'theme-textdomain' ),
		'id' => 'social_cc',
		'std' => '#',
		'type' => 'text'
	);

	/* PUBLICIDAD */
	$options[] = array(
		'name' => __( 'Publicidad', 'theme-textdomain' ),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __( 'Publicidad', 'theme-textdomain' ),
		'desc' => __( 'Añade tu código de Google Adsense. La publicidad tiene que ser de 728x15 px. Aparece entre el menú y el contenido', 'theme-textdomain' ),
		'id' => 'ads',
		'std' => '',
		'type' => 'textarea'
	);

	/* OTROS */
	$options[] = array(
		'name' => __( 'Otros', 'theme-textdomain' ),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __( 'Activar Navidad :D', 'theme-textdomain' ),
		'desc' => __( 'Añade un árbol de navidad en el footer', 'theme-textdomain' ),
		'id' => 'christmas',
		'std' => '0',
		'type' => 'checkbox'
	);

	return $options;
}
