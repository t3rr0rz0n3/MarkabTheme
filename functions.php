<?php
/* MAIN FUNCTIONS */

// Theme Options
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/NunkiCore/' );
require_once dirname( __FILE__ ) . '/inc/NunkiCore/options-framework.php';
require_once dirname( __FILE__ ) . '/inc/NunkiCore/nunki-options.php';

// Loads options.php from child or parent theme
$optionsfile = locate_template( 'options.php' );
load_template( $optionsfile );

/* MAIN FUNCTIONS */

// Register Custom Navigation Walker and add menu
require_once('wp_bootstrap_navwalker.php');


register_nav_menus( array(
    'primary' => __( 'Menu Superior', 'Alpheratz Theme' ),
) );

// Active thumbnails
add_theme_support('post-thumbnails');
add_image_size('list_articles_thumbs', 350, 270, true );
the_post_thumbnail();

the_post_thumbnail('thumbnail');    // Thumbnail (default 150px x 150px max)
the_post_thumbnail('medium');       // Medium resolution (default 300px x 300px max)
the_post_thumbnail('large');        // Large resolution (default 640px x 640px max)
the_post_thumbnail('full');         // Original image resolution (unmodified)

//eliminar codigo basura de cabecera
/*
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
//Automatically move JavaScript code to page footer, speeding up page loading time.
remove_action('wp_head', 'wp_print_scripts');
remove_action('wp_head', 'wp_print_head_scripts', 9);
remove_action('wp_head', 'wp_enqueue_scripts', 1);
add_action('wp_footer', 'wp_print_scripts', 5);
add_action('wp_footer', 'wp_enqueue_scripts', 5);
add_action('wp_footer', 'wp_print_head_scripts', 5);
*/



// Add sidebar
register_sidebar(array(
    'name' => 'SideBar',
    'before_widget' => '<div class="widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Add sidebar on footer
register_sidebar(array(
    'name' => 'Footer',
    'before_widget' => '<div class="widget col-md-4">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Add Social Network on profile
function alpheratzAuthorSocialIcons( $contactmethods ) {

    $contactmethods['twitter'] = 'Twitter URL';
    $contactmethods['facebook'] = 'Facebook URL';
    $contactmethods['googleplus'] = 'Google + URL';
    $contactmethods['correo'] = 'Correo Electrónico';
    $contactmethods['github'] = 'GitHub URL';
    /*$contactmethods['gnusocial'] = 'GNU Social URL';
    $contactmethods['diaspora'] = 'Diaspora* URL';*/

    return $contactmethods;
}
add_filter( 'user_contactmethods', 'alpheratzAuthorSocialIcons', 10, 1);

// Social Network in user profile
function alperatzListSocialNetwork() {

    echo '<ul class="social-author">';
    $twitter = get_the_author_meta( 'twitter' );
    if ( $twitter && $twitter != '' ) {
        echo '<li><a target="_blank" class="icon-social tw" href="' . esc_url($twitter) . '"><i class="fa fa-twitter"></i></a></li>';
    }

    $facebook = get_the_author_meta( 'facebook' );
    if ( $facebook && $facebook != '' ) {
        echo '<li><a target="_blank" class="icon-social fb" href="' . esc_url($facebook) . '"><i class="fa fa-facebook"></i></a></li>';
    }

    $googleplus = get_the_author_meta( 'googleplus' );
    if ( $googleplus && $googleplus != '' ) {
        echo '<li><a target="_blank" class="icon-social gp" href="' . esc_url($googleplus) . '"><i class="fa fa-google-plus"></i></a></li>';
    }

    $correo = get_the_author_meta( 'correo' );
    if ( $correo && $correo != '' ) {
        echo '<li><a target="_blank" class="icon-social ce" href="mailto:' . esc_url($correo) . '"><i class="fa fa-envelope"></i></a></li>';
    }

    $github = get_the_author_meta( 'github' );
    if ( $github && $github != '' ) {
        echo '<li><a target="_blank" class="icon-social gh" href="' . esc_url($github) . '"><i class="fa fa-github-alt"></i></a></li>';
    }

    /*$gnusocial = get_the_author_meta( 'gnusocial' );
    if ( $gnusocial && $gnusocial != '' ) {
        echo '<li><a target="_blank" class="gs" href="' . esc_url($gnusocial) . '"><span class="sociconoff">GNUSocial</span></a></li>';
    }

    $diaspora = get_the_author_meta( 'diaspora' );
    if ( $diaspora && $diaspora != '' ) {
        echo '<li><a target="_blank" class="dp" href="' . esc_url($diaspora) . '"><span class="sociconoff">Diaspora</span></a></li>';
    }*/
    echo '</ul>';
}

// Add image header with effect parallax
function getImageHeader() {
    // Si es index.php
    if (is_home()) {
        $url = bloginfo('template_url') . "/img/headerbg.png";
        return $url;
    } else if (is_single()) {
        $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        return $url;
    }
}

function getNavbarClass() {
    // Si es index.php
    if (is_home()) {
        $classes = 'navbar';
        return $classes;
    } else if (is_single()) {
        $classes = 'navbar navbar-fixed-top';
        return $classes;
    }
}

// Social Network in footer
function alperatzSocialNetworkFooter() {
    $tw = of_get_option( 'social_tw', '#' );
    if (of_get_option('social_tw') != "" ) {
        echo "<li><a target='_blank' class='tw' href=" . "$tw" . "><i class='fa fa-twitter'></i></a></li>";
    }

    $fb = of_get_option( 'social_fb', '#' );
    if (of_get_option('social_fb') != "" ) {
        echo "<li><a target='_blank' class='fb' href=" . "$fb" . "><i class='fa fa-facebook'></i></a></li>";
    }

    $gp = of_get_option( 'social_gp', '#' );
    if (of_get_option('social_gp') != "" ) {
        echo "<li><a target='_blank' class='gp' href=" . "$gp" . "><i class='fa fa-google-plus'></i></a></li>";
    }

    $gh = of_get_option( 'social_gh', '#' );
    if (of_get_option('social_gh') != "" ) {
        echo "<li><a target='_blank' class='gh' href=" . "$gh" . "><i class='fa fa-github-alt'></i></a></li>";
    }
    $tg = of_get_option( 'social_tg', '#' );
    if (of_get_option('social_tg') != "" ) {
        echo "<li><a target='_blank' class='tg' href=" . "$tg" . "><i class='fa fa-paper-plane'></i></a></li>";
    }

    $rss = of_get_option( 'social_rs', '#' );
    if (of_get_option('social_rs') != "" ) {
        echo "<li><a target='_blank' class='rss' href=" . "$rss" . "><i class='fa fa-rss'></i></a></li>";
    }

    $cc = of_get_option( 'social_cc', '#' );
    if (of_get_option('social_cc') != "" ) {
        echo "<li><a target='_blank' class='ccs' href=" . "$cc" . "><i class='fa fa-creative-commons'></i></a></li>";
    }
    //<li><a target="_blank" class="ins" href="https://instagram.com/zagurito/"><span class="socicon">x</span></a></li>
}

function oldPosts() {

    // Variables, a year, 2 year, 3 year, 4 year, more than 5 years
    $time_old_year = 60*60*24*365;
    $time_old_2year = 60*60*24*365*2;
    $time_old_3year = 60*60*24*365*3;
    $time_old_4year = 60*60*24*365*4;
    $time_old_MoreYear = 60*60*24*365*5;
    $time_now = date('U')-get_the_time('U');

    if ($time_now > $time_old_year && $time_now < $time_old_2year) {
        echo '<div class="alert alert-success" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace un año.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_2year && $time_now < $time_old_3year) {
        echo '<div class="alert alert-info" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace dos años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_3year && $time_now < $time_old_4year) {
        echo '<div class="alert alert-warning" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace tres años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_4year && $time_now < $time_old_MoreYear) {
        echo '<div class="alert alert-danger" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace cuatro años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    } elseif ($time_now > $time_old_MoreYear) {
        echo '<div class="alert alert-black" role="alert">
            <strong>ATENCIÓN: Este artículo fue publicado hace más de cinco años.</strong><br />
                Es posible que este artículo contenga información desfasada.
            </div>';
    }
}

// Posts relacionados (en pruebas)
function AlpheratzRelatedAuthorPosts() {
    global $authordata, $post;
    $authors_posts = get_posts( array( 'author' => $authordata->ID, 'post__not_in' => array( $post->ID ), 'posts_per_page' => 6 ) );
    $output = '<ul>';
    foreach ( $authors_posts as $authors_post ) {
        $output .= '<li class><a href="' . get_permalink( $authors_post->ID ) . '">' . apply_filters( 'the_title', $authors_post->post_title, $authors_post->ID ) . '</a></li>';
    }
    $output .= '</ul>';
    return $output;
}

/**
 * Load javascripts used by the theme
 */
/*
function AlpheratzInfiniteScrollJS(){
	wp_register_script( 'infinite_scroll',  get_template_directory_uri() . '/js/jquery.infinitescroll.min.js', array('jquery'),null,true );
	if( ! is_singular() ) {
		wp_enqueue_script('infinite_scroll');
	}
}
add_action('wp_enqueue_scripts', 'AlpheratzInfiniteScrollJS');
*/
/**
 * Infinite Scroll
 *//*
function AlpheratzInfiniteScroll() {

    $stateCheckboxInfiniteScroll = of_get_option('infinitescroll');

    if ($stateCheckboxInfiniteScroll == 1) {
        if( ! is_singular() ) { ?>
    	<script>
    	var infinite_scroll = {
    		loading: {
    			img: "<?php echo get_template_directory_uri(); ?>/img/loading.gif",
    			msgText: "<?php _e( 'Cargando entradas...', 'custom' ); ?>",
    			finishedMsg: "<?php _e( 'Has llegado al final!', 'custom' ); ?>",
                animate : true // if the page will do an animated scroll when new content loads
    		},
    		"nextSelector":"li a.next",
    		"navSelector":".navigation",
    		"itemSelector":"article",
    		"contentSelector":"#main"
    	};
    	jQuery( infinite_scroll.contentSelector ).infinitescroll( infinite_scroll );
    	</script>
    	<?php
    	}
    }
}
add_action( 'wp_footer', 'AlpheratzInfiniteScroll',200 );
*/


// Mostrar los posts populares en single-content

function popularPosts() {

    $mipost = $post;
    $args = array( 'numberposts' => 3, 'orderby' => 'rand', 'post_status' => 'publish', 'offset' => 1);
    $rand_posts = get_posts( $args );

    foreach ($rand_posts as $post) {

        $id = $post->ID;
        $title = $post->post_title;
        $titlePrint = substr($title, 0, 26) . "...";

        echo '<li class="trendingPost">';
        echo '<a data-toggle="tooltip" data-placement="bottom" href="' . get_permalink($id) . '" title="' . $title . '">' . $titlePrint . '</a> ';
        echo '</li>';
    }
}

/* Modificar el tamaño del exceprt */
function getExcerpt($count){
    $permalink = get_permalink($post->ID);
    $excerpt = get_the_content();
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    //$excerpt = $excerpt.'... <a href="'.$permalink.'">leer mas</a>';
    return $excerpt;
}

?>
