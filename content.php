<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-12'); ?>>
	<div class="blog-item-wrap square">
		<div class="time col-md-2">
			<span><?php the_time('d M') ?></span>
			<span><?php the_time("Y") ?></span>
		</div>
		<h1 class="entry-title col-md-10">
			<a href="<?php the_permalink(); ?>" alt="<?php the_title_attribute(); ?>" title="<?php the_title_attribute(); ?>" >
				<?php the_title(); ?>
			</a>
		</h1><!-- .entry-title -->
		<div class="row">
			<div class="col-md-6">
				<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
					<div class="thumb">
						<!-- Si tiene thumb por defecto, se muestra el thumb con tamaño "medium" (ver documentación en functions)
					 		 Si no tiene un thumb por defecto, entonces carga una imagen por defecto. -->
						<?php if ( has_post_thumbnail() ) {
							the_post_thumbnail('medium', array('class' => 'img-responsive'));
						} else { ?>
								<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/img/default-thumb.png" alt="<?php the_title(); ?>" />
						<?php } ?>

					</div><!-- img -->
				</a>
			</div>
			<div class="col-md-6">
				<p class="excerpt">
					<?php  echo getExcerpt(500) . ' [...]'; ?>
				</p>
			</div>
		</div>
	</div><!-- square -->

	<div class="post-inner-content">
		<header class="entry-header page-header">

				<?php if ( 'post' == get_post_type() ) : ?>
					<div class="entry-meta row">
						<div class="col-md-8">
							<ul class="info">
								<li>
									<span class="fa fa-user"> </span>
									<a title="<?php the_author(); ?>" alt="<?php the_author(); ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?></a>
								</li>
								<li>
									<span class="fa fa-tags"></span>
									<?php
										$category = get_the_category();
										if ($category) {
										  echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
									  }
									?>
								</li>
								<li>
									<span class="fa fa-comment"></span>
									<?php
					                    comments_number(
					                    '<span>' . __("Ningún","wpbootstrap") . '</span> ' . __("comentario","wpbootstrap") . '',
					                    '<span>' . __("Un","wpbootstrap") . '</span> ' . __("comentario","wpbootstrap") . '',
					                    '<span>%</span> ' . __("comentarios","wpbootstrap") );?>
					                <?php _e("","wpbootstrap"); ?>
								</li>
								<li>
									<?php edit_post_link( __( 'Edit' ), '<span class="glyphicon glyphicon-edit"></span> <span class="edit-link">', '</span>' ); ?>
								</li>
							</ul><!-- .info -->
						</div><!-- col-md-8 -->
						<div class="col-md-4 text-right read-more">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" >
								<i class="fa fa-external-link" aria-hidden="true"></i>
								Leer más
							</a>
						</div>
					</div><!-- .entry-meta -->
				<?php endif; ?>
			</header><!-- .entry-header -->

			<?php if ( is_search() ) : // Only display Excerpts for Search ?>
				<div class="entry-summary">
					<?php the_excerpt(); ?>
					<p><a title="<?php the_title_attribute(); ?>" alt="<?php the_title_attribute(); ?>" class="btn btn-default read-more" href="<?php the_permalink(); ?>"><?php _e( 'Leer más' ); ?></a></p>
				</div><!-- .entry-summary -->
			<?php else : ?>
				<!-- aquí es pot ficar contingut -->
			<?php endif; ?>
		</div>
</article><!-- #post-## -->
