<?php $search_terms = htmlspecialchars( $_GET["s"] ); ?>
<form role="form" action="<?php bloginfo('siteurl'); ?>/" id="searchform" method="get">
    <label for="s" class="sr-only">Buscar</label>
    <input type="text" class="form-control" id="s" name="s" placeholder="Buscar"<?php if ( $search_terms !== '' ) { echo ' value="' . $search_terms . '"'; } ?> />
</form>
