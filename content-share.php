<div class="row">
    <div class="share-post">

        <h4><?php _e('Compartir', 'Alpheratz Theme');?></h4>
        <ul class="share-buttons">
            <!-- FACEBOOK -->
            <li>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" title="Compartir con Facebook" target="_blank">
                     <i class="fa fa-facebook"></i>
                </a>
            </li>
            <!-- TWITTER -->
            <li>
                <a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>%20%23PortalLinux&via=zagurito" target="_blank" title="Twitea!">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <!-- Google+ -->
            <li>
                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" title="Compartir en Google+!">
                    <i class="fa fa-google-plus"></i>
                </a>
            </li>
            <!-- Email -->
            <li>
                <a href="mailto:?subject=<?php the_title(); ?>&body=<?php the_title(); ?>:<?php the_permalink(); ?>" target="_blank" title="Envía un correo!">
                    <i class="fa fa-envelope-o"></i>
                </a>
            </li>
            <!--
            <li class="dropdown">
                <a href="#" class="social social-gp dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <div class="social social-ot col-md-3">
                        <div class="logo col-md-3">
                             <i class="fa fa-google-plus"></i>
                        </div>
                        <div class="name col-md-9">
                            <span>Otros</span>
                        </div>
                    </div>
                </a>
                <ul class="dropdown-menu" aria-labelledby="dropdownmenu1">


                </ul>
            </li>


            <li>
                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" title="Compartir en Google+">
                    <img src="<?php bloginfo('template_url')?>/img/iconsSocial/Google+.svg">
                </a>
            </li>
            <li>
                <a href="http://www.reddit.com/submit?url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" title="Envíar a Reddit">
                    <img src="<?php bloginfo('template_url')?>/img/iconsSocial/Reddit.svg">
                </a>
            </li>-->

        </ul>
    </div> <!-- /. share-post -->
</div><!-- .row -->
