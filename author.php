<?php get_header(); ?>
                            <div id="primary" class="col-md-8 page" <?php echo alpheratz_SidebarPosition() ?>>
                                <main id="main">
                                    <?php
                                        get_template_part( 'content-author', get_post_format() );
                                    ?>
                                </main>
                            </div><!-- #primary -->

                            <div id="secondary" class="col-md-4">
                                <?php get_sidebar(); ?>
                            </div><!-- #secondary -->
<?php get_footer(); ?>
