<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="keywords" content="Software Libre, Stallman, Linux, GNU, GNU/Linux">
        <meta name="author" content="Jesús Camacho">
        <title><?php if ( is_single() ) { } ?> <?php wp_title(); ?></title>

        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

        <!-- RSS -->
        <link rel="alternate" type="aplication/rss+xml" title="PortalLinux" href="http://www.portallinux.es/feed" />

        <!-- FAVICON -->
        <link rel="shortcut icon" href="<?php echo of_get_option( 'favicon' ); ?>" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <!-- Styles -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/normalize.min.css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/bootstrap.min.css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/cc-icons.css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/style.min.css" media="screen" charset="utf-8">

        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=News+Cycle:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,700' rel='stylesheet' type='text/css'>

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); echo alpheratz_ChangeBackgroundImageOrColor(); ?>>
        <main id="page">
            <header class="parallax-container" data-bleed="20" data-natural-height="900" data-natural-width="1400" data-bleed="62" data-parallax="scroll" data-image-src="<?php echo getImageHeader(); ?>">
                <div class="test">
                    <!--<div class="social-list">
                        <ul class="social-network">
                            <?php alperatzSocialNetworkFooter() ?>
                        </ul>
                    </div>-->
                    <div class="header-inner section-inner">
                        <h1 class="site-title">
                        <?php
                            if (is_home()) {
                                bloginfo( 'name' );
                            } else if (is_single()) {
                                the_title();
                            }

                        ?>
                        </h1>
                        <h3 class="site-description">
                            <?php
                                if (is_home()) {
                                    bloginfo( 'description' );
                                } else if (is_single()) {
                                    alpheratz_ShareContent();
                                }
                            ?>
                        </h3>
                    </div>
                </div>
                <nav class="<?php echo getNavbarClass() ?>">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed buscador">
                            <a href="#searchBox" title="Buscar">
                                <span class="fa fa-search" aria-hidden="true"></span>
                            </a>
                        </button>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a title="<?php bloginfo( 'name' ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <span class="glyphicon <?php echo of_get_option( 'glyphicon', 'glyphicon glyphicon-flash' ); ?>"></span>
                            <span class="first"><strong><?php echo of_get_option('firstword', 'Markab'); ?></strong></span>
                            <span class="second"><strong><?php echo of_get_option('secondword', 'Theme'); ?></strong></span>
                        </a>
                    </div><!-- .narbar-header -->

                        <?php
                           wp_nav_menu( array(
                                'menu'              => 'primary',
                                'theme_location'    => 'primary',
                                'depth'             => 2,
                                'container'         => 'div',
                                'container_class'   => 'collapse navbar-collapse',
                                'container_id'      => 'bs-example-navbar-collapse-1',
                                'menu_class'        => 'nav navbar-nav navbar-right',
                                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                'walker'            => new wp_bootstrap_navwalker()
                            ));
                        ?>

                </nav>
            </header>
            <!--<div id="pseudomenu"></div>--><!-- #pseudomenu -->
            <div id="content">

                <div id="anuncio">
                    <?php echo of_get_option( 'ads', '#' ); ?>
                </div><!-- #anuncio -->

                <!-- BUSCADOR -->
        		<div id="box_animate" class="col-md-12">
                    <div class="right-inner-addon">
               			<aside id="search" class="widget widget_search">
        					<?php get_search_form(); ?>
        				</aside>
            		</div>
        		</div><!-- #box_animate -->

                <div class="container main-content-area">
                    <div class="row">
                        <div id="content" class="col-md-12 col-sm-12 contentPost">
