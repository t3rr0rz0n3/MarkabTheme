<?php get_header(); ?>
                            <div id="primary" class="home col-md-8">
                                <main id="main">
                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                        <?php
                                            get_template_part( 'content');
                                        ?>

                                    <?php endwhile; else: ?>

                                        <div class="">
                                            <h1>
                                                Aún no hay artículos para cargar
                                            </h1>
                                        </div>

                                    <?php endif; ?>
                                    <nav class="navigation paging-navigation" role="navigation">
                                        <div class="nav-links">
                                            <!--< ? php AlpheratzPagination(); ? >-->
                                        <div><!-- .nav-links -->
                                    </nav><!-- .navigation -->
                                </main>
                            </div><!-- #primary -->

                            <div id="secondary" class="col-md-4">
                                <?php get_sidebar(); ?>
                            </div><!-- #secondary -->
<?php get_footer(); ?>
