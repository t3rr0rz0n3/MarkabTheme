                            </div><!-- #content -->
                        </div><!-- .row -->
                    </div><!-- .container .main-content-area -->
                </div><!-- #content -->
            <div id="footer-area">
                <div class="container footer-inner">
                    <div class="row">
                        <div class="foother-widget-area">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer') ) : endif; ?>
                        </div>
                    </div>
                </div>
                <div id="colophon" class="site-footer">
                    <div class="col-md-4 copyright">
                        <?php alpheratz_totoplink(); ?>
                        <a title="<?php wp_title(); ?>" alt="" href="<?php echo esc_url( home_url( '/' ) ); ?>" ><?php echo of_get_option( 'copyright', 'Nombre Autor' ); ?> - <?php printf(__('%s','Alpheratz'), date('Y')); ?></a>
                    </div>
                    <div class="col-md-4 social">

                    </div>
                    <div class="col-md-4 themeName">
                        <a title="PortalLinux" href="https://github.com/Zagur/AlpheratzTheme"><font color="F1F1F1"><strong>Alpheratz</strong></font><strong><span class="second">Theme</span></strong></a> por
                        <a title="Jesús Camacho" href="http://www.entrebits.org">Jesús Camacho</a>
                    </div><!-- .themeName -->
                </div><!-- #colophon .site-footer -->
            </div><!-- #footer-area -->
            <footer>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="copyleft">
								© <script>
									date=new Date();
									anyo=date.getFullYear();
									document.write(anyo);
								</script>
								Libro De Trucos.
								<span> || </span>
								<a href="">Jesús Camacho</a>
							</p>
                        </div>
                    </div>
                </div>
            </footer>
            <?php alpheratz_christmas(); ?>
            <?php alpheratz_cookies(); ?>
        </main><!-- #page -->

        <!-- Scripts -->
        <script src="<?php bloginfo('template_url')?>/js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url')?>/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url')?>/js/parallax.min.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url')?>/js/alpheratz.min.js" type="text/javascript"></script>


        <?php wp_footer(); ?>
    </body>
</html>
