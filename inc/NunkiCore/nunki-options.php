<?php

/**
 * Nunki Core
 *
 * @package   Options Framework
 * @author    Jesús Camacho <zagurblog@gmail.com>
 * @license   GPL-3v
 * @link      http://zagur.github.io/nunkicore
 *
 * @wordpress-plugin
 * Plugin Name: Nunki Core
 * Plugin URI:  http://zagur.github.io/nunkicore
 * Description: A framework for building theme options.
 * Version:     1.0
 * Author:      Jesús Camacho
 * Author URI:  http://zagur.github.io
 * License:     GPL-3v
 * License URI: http://www.gnu.org/licenses/gpl-3.0.txt
 * Text Domain: nunkicore, optionsframework
 */

//
// CONFIGURACION BASICA
//

/* Glyphicons */
/*
<?php echo of_get_option( 'glyphicon', 'glyphicon glyphicon-flash' ); ?>
*/

/* Título principal de la web: Primera palabra */
/*
<?php echo of_get_option('firstword', 'Test'); ?>
*/

/* Título principal de la web: Segunda palabra */
/*
<?php echo of_get_option('secondword'); ?>
*/

/* Activar aviso cookies */
function alpheratz_cookies() {
    $stateCheckboxCookies = of_get_option('cookies');

    if ($stateCheckboxCookies == 1) {
        echo '<div class="cookies" ondblclick="cerrarCookie(this);"><span id="cookie" tabindex="0" class="glyphicon glyphicon-cog" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="left" data-content="Utilizamos cookies propias y de terceros para mejorar nuestros servicios. Si continúa navegando, consideramos que acepta su uso. Doble clic sobre el triángulo rojo para cerrar."></span></div>';
    }
}

/* Activar Breadcrumbs */
function alpheratz_breadcrumb() {
    $stateCheckboxBreadCrumb = of_get_option('breadcrumb');

    if ($stateCheckboxBreadCrumb == 1) {
        if (!is_home()) {
            echo '<span class="glyphicon glyphicon-home"></span>';
    		echo '<span> ';
    		bloginfo('name');
    		echo '</span> <span class="fa fa-angle-right"></span> ';
    		if (is_category() || is_single()) {
    			the_category('title_li=');
    			if (is_single()) {
    				echo ' <span class="fa fa-angle-right"></span> ';
    				the_title();
    			}
    		} elseif (is_page()) {
    			echo the_title();
    		}
    	}
    }
}

/* Activar "Subir Arriba" */
function alpheratz_totoplink() {
    $stateCheckBoxToTop = of_get_option('totop');

    if ($stateCheckBoxToTop == 1) {
        echo '<a id="backtotop" href="#" onclick="toTopEffect();return false"><span class="glyphicon glyphicon-chevron-up"></span></a>';
    }
}

/* Activar "Infinite Scroll" */



/* Activar "Lo más visto" */
function alpheratz_TrendingArticles() {
    $stateCheckTrendingArticles = of_get_option('trendpost');

    if ($stateCheckTrendingArticles == 1) {
        echo '<div class="trending-articles">';
        echo '<ul>';
        echo '<li class="firstlink">Lo más visitado: </li>';
        echo popularPosts();
        echo '</ul>';
        echo '</div>';
    }
}

/* Activar "Compartir con redes sociales" */
function alpheratz_ShareContent() {
    $stateCheckShareContent = of_get_option('sharecontent');

    if ($stateCheckShareContent == 1) {
        get_template_part('content', 'share');
    }
}

//
// CONFIGURACION AVANZADA
//

/* Posición de la barra lateral */
function alpheratz_SidebarPosition() {
    $position = of_get_option('sidebar_position');
    if ($position == "sidebar_left") {
        $stylePosition = 'style="float:right"';
        return $stylePosition;
    }
}

/* Style */
/*
<?php echo of_get_option( 'style', 'style.min.css' ); ?>
*/

/* Fondo del Tema */
function alpheratz_ChangeBackgroundImageOrColor() {
    $background = of_get_option('example_background');
    if ( $background ) {
        if ( $background['image'] ) {
            $bgc = ' style="background:url(' . $background['image'] . ')"';
            return $bgc;
        } else if ( $background['color'] ) {
            $bgc = ' style="background:' . $background['color'] . '"';
            return $bgc;
        } else {
            return null;
        }
    }
}

/* Favicon */
/*
<?php echo of_get_option( 'favicon' ); ?>
*/

// TIPOGRAFIA
/* Developing */

//
// LICENCIA
//

/* Selecciona tu licencia */
function alpheratz_license() {
    $multicheck = of_get_option( 'license', 'none' );

    if ( is_array( $multicheck ) ) {
        foreach ( $multicheck as $key => $value ) {
            // If you need the option's name rather than the key you can get that
            $name = $test_array_jr[$key];
            // Prints out each of the values
            if ($value == 1) {
                echo '<span class="cc ' . $key . '"></span>';
            }
        }
    }
    else {
        echo '<span>No has seleccionado ninguna licencia</span>';
    }
}

// FOOTER

/* Copyright */


/*


// Social Network in footer
function alperatzSocialNetworkFooter() {
    $tw = of_get_option( 'social_tw', '#' );
    if (of_get_option('social_tw') != "" ) {
        echo "<li><a target='_blank' class='tw' href=" . "$tw" . "><i class='fa fa-twitter'></i></a></li>";
    }

    $fb = of_get_option( 'social_fb', '#' );
    if (of_get_option('social_fb') != "" ) {
        echo "<li><a target='_blank' class='fb' href=" . "$fb" . "><i class='fa fa-facebook'></i></a></li>";
    }

    $gp = of_get_option( 'social_gp', '#' );
    if (of_get_option('social_gp') != "" ) {
        echo "<li><a target='_blank' class='gp' href=" . "$gp" . "><i class='fa fa-google-plus'></i></a></li>";
    }

    $gh = of_get_option( 'social_gh', '#' );
    if (of_get_option('social_gh') != "" ) {
        echo "<li><a target='_blank' class='gh' href=" . "$gh" . "><i class='fa fa-github-alt'></i></a></li>";
    }
    $tg = of_get_option( 'social_tg', '#' );
    if (of_get_option('social_tg') != "" ) {
        echo "<li><a target='_blank' class='tg' href=" . "$tg" . "><i class='fa fa-paper-plane'></i></a></li>";
    }

    $rss = of_get_option( 'social_rs', '#' );
    if (of_get_option('social_rs') != "" ) {
        echo "<li><a target='_blank' class='rss' href=" . "$rss" . "><i class='fa fa-rss'></i></a></li>";
    }

    $cc = of_get_option( 'social_cc', '#' );
    if (of_get_option('social_cc') != "" ) {
        echo "<li><a target='_blank' class='ccs' href=" . "$cc" . "><i class='fa fa-creative-commons'></i></a></li>";
    }
    //<li><a target="_blank" class="ins" href="https://instagram.com/zagurito/"><span class="socicon">x</span></a></li>
}

// PUBLICIDAD


//
// OTROS
//

/* Navidad */
function alpheratz_christmas() {
    $stateCheckboxChristmas = of_get_option('christmas');
    $imgTree =  get_template_directory_uri() . '/inc/NunkiCore/images/christmas-tree-icon.png'; // CAMBIAR ESTA RUTA

    if ($stateCheckboxChristmas == 1) {
        echo '<div class="christmas"><img title="Árbol de navidad" alt="Árbol de navidad" src="' . esc_url($imgTree) . '"></div>';
    }
}


/* ------------------------------
FUNCTIONS NUNKI OPTIONS --------------------*/
// Para que script i ins funcionen en Options Theme
add_action('admin_init','optionscheck_change_santiziation', 100);

function optionscheck_change_santiziation() {
    remove_filter( 'of_sanitize_textarea', 'of_sanitize_textarea' );
    add_filter( 'of_sanitize_textarea', 'custom_sanitize_textarea' );
}

function custom_sanitize_textarea($input) {
    global $allowedposttags;
    $custom_allowedtags["ins"] = array(
        "class" => array(),
        "style" => array(),
        "data-ad-client" => array(),
        "data-ad-slot" => array()
      );
      $custom_allowedtags["script"] = array(
          "src" => array(),
          "type" => array(),
      );

      $custom_allowedtags = array_merge($custom_allowedtags, $allowedposttags);
      $output = wp_kses( $input, $custom_allowedtags);
      return $output;
}

?>
